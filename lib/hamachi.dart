/// Dart support for accessing information about Hamachi networks and clients
///
/// Use the Hamachi class to access the static methods
///
/// Get a list of all computers in all your hamachi networks or a specific one
///
/// ```
/// Hamachi.getList()
/// Hamachi.getList(network)
/// ```
///
/// Get a list of all of your hamachi networks
///
/// ```
/// Hamachi.getNetworkList()
/// ```
///
/// Get the details about a hamachi computer
///
/// ```
/// Hamachi.getHamachiComputer(hamid)
/// ```
///
/// Get the status of a hamachi computer
///
/// ```
/// Hamachi.getHamachiStatus(hamid)
/// ```
///
library hamachi;

export 'src/hamachi_base.dart';
export 'src/hamachi_computer.dart';
export 'src/hamachi_network.dart';
export 'src/hamachi_connection.dart';
export 'src/hamachi_status.dart';
