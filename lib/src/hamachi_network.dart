/// Model to hold information about a single Hamachi network
///
///
/// This information is parsed from calling `hamachi list`
/// ```
/// hamachi list
///
///  * [222-222-222]Network1         capacity: 4/5, subscription type: Free, owner: owner@mail.com
///      * 222-222-111   one                        25.xxx.xx.xxx     alias: not set           2620:xx::xxxx:xxxx                          direct      UDP  24.xxx.xxx.xx:xxxxx
///      * 232-232-222   two                        25.xx.xxx.xxx     alias: not set           2620:xx::xxxx:xxxx                          direct      UDP  24.xxx.xxx.xx:xxxxx
///        233-245-333   three                      25.xx.xx.xxx      alias: not set
///  * [333-333-333]Network2  capacity: 4/5, subscription type: Free, owner: owner@mail.com
///      * 232-216-111   uno                        25.x.xx.xxx       alias: not set           2620:xx::xxxx:xxxx                          direct      UDP  24.xxx.xxx.xxx:xxxxx
///      * 232-216-222   dos                        25.x.xxx.xx       alias: not set           2620:xx::xxxx:xxxx                          direct      UDP  24.xxx.xxx.xxx:xxxxx
///      * 232-216-333   tres                       25.x.x.xxx        alias: not set           2620:xx::xxxx:xxx                           direct      UDP  24.xxx.xxx.xxx:xxxxx
/// ```
/// The network information can be obtained via the computer
/// ```
/// var computer = Hamachi.getHamachiComputer('222-222-111');
///
/// /*
/// computer.network.id == '222-222-222'
/// computer.network.name == 'Network1'
/// computer.network.capacity == '4/5'
/// computer.network.type == 'Free'
/// computer.network.owner == 'owner@mail.com'
/// */
/// ```
class HamachiNetwork {
  /// id corresponds to the hamachi network id (222-222-222)
  String id;

  /// name corresponds to the name of the hamachi network (network1)
  String name;

  /// capacity describes the current capacity of the network (3/5)
  String capacity;

  /// type describes the type of network (Free/Standard)
  String type;

  /// owner corresponds to the owner of the network (owner@email.com)
  String owner;

  /// Constructor
  ///
  HamachiNetwork();

  /// Creates a HamachiNetwork object from a regex match
  ///
  factory HamachiNetwork.fromMatch(RegExpMatch match) {
    var network = HamachiNetwork();
    network.id = match.namedGroup('id');
    network.name = match.namedGroup('name').trim();
    network.capacity = match.namedGroup('capacity').trim();
    network.type = match.namedGroup('type').trim();
    network.owner = match.namedGroup('owner').trim();
    // print('\n ${network}');
    return network;
  }
  @override
  String toString() {
    return 'HamachiNetwork: ${id} ${name} ${capacity} ${type} ${owner}';
  }
}
