/// Model to hold information about the type of Hamachi connection
///
class HamachiConnection {
  static final reConnection = RegExp(
      r'^  (?<active>[ *]) (?<type>.{12})(?<status>.{9})(?<protocol>TCP|UDP). (?<address>.*)$');

  /// active shows if the hamachi client is currently connected (true/false)
  bool active;
  String type;
  String status;
  String protocol;
  String address;

  /// Constructor
  HamachiConnection();

  /// Builds a connection from a line output of Hamachi
  ///
  factory HamachiConnection.fromString(String line) {
    var connection;
    var match = reConnection.firstMatch(line);
    if (match != null) {
      connection = HamachiConnection();
      connection.active = match.namedGroup('active') == '*';
      connection.type = match.namedGroup('type').trim();
      connection.status = match.namedGroup('status').trim();
      connection.protocol = match.namedGroup('protocol').trim();
      connection.address = match.namedGroup('address').trim();
    }
    return connection;
  }
  @override
  String toString() {
    return 'HamachiConnection: ${active} ${type} ${status} ${protocol} ${address}';
  }
}
