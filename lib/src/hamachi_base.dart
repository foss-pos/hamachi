import 'dart:io';
import 'package:hamachi/hamachi.dart';
import 'package:hamachi/src/hamachi_computer.dart';
import 'package:hamachi/src/hamachi_network.dart';
import 'package:hamachi/src/hamachi_status.dart';

/// Main class used to access all the static methods
///
class Hamachi {
  static final reNetwork = RegExp(
      r'^ [\* ] \[(?<id>\d{3}-\d{3}-\d{3})\](?<name>.*)capacity: (?<capacity>[^,]*), subscription type: (?<type>[^,]*), owner: (?<owner>.*)$');
  static final reComputer = RegExp(
      r'^[ ]{5}(?<status>[\* ]) (?<id>\d{3}-\d{3}-\d{3}) {3}(?<name>.{27})(?<ipv4>[\d\. ]{18})alias: (?<alias>.{18})(?:(?<ipv6>.{18})(?:(?:.{26})(?<type>.{12})(?<protocol>.{3})(?<proxy>.*)?)?)?');

  /// Get a List of computers in your hamachi networks.
  ///
  static List<HamachiComputer> getList([String networkName = 'all']) {
    var list = <HamachiComputer>[];
    var lines = _getHamachiList().split('\n');
    // print(lines);
    HamachiNetwork network;
    lines.forEach((line) {
      var match = reNetwork.firstMatch(line);
      if (match != null) {
        // first attempt to match a network
        network = HamachiNetwork.fromMatch(match);
      } else {
        // if not network it must be a computer
        match = reComputer.firstMatch(line);
        if (match != null) {
          try {
            var computer = HamachiComputer.fromMatch(match);
            computer.network = network;
            list.add(computer);
          } catch (error) {
            print('Error parsing ${line}');
          }
        }
      }
    });
    if (networkName != 'all') {
      list =
          list.where((element) => element.network.name == networkName).toList();
    }
    // print(list);
    return list;
  }

  /// Get a List of all hamachi networks
  ///
  static List<HamachiNetwork> getNetworkList() {
    var list = <HamachiNetwork>[];
    var lines = _getHamachiList().split('\n');
    lines.forEach((line) {
      var match = reNetwork.firstMatch(line);
      if (match != null) {
        var network = HamachiNetwork.fromMatch(match);
        list.add(network);
      }
    });
    return list;
  }

  /// Get the connected status of a Hamachi client
  ///
  static HamachiStatus getHamachiStatus(String hamId) {
    HamachiStatus peerStatus;
    if (hamId.isNotEmpty) {
      var peerString = _getHamachiPeer(hamId);
      peerStatus = HamachiStatus.fromList(peerString.split('\n'));
      // print(peerStatus);
    }
    return peerStatus;
  }

  /// Get the details about a hamachi client
  ///
  static HamachiComputer getHamachiComputer(String hamId) {
    var hamComputer = getList().firstWhere((element) => element.id == hamId);
    return hamComputer;
  }

  /// Login to Hamachi network
  ///
  static String Login() {
    return _exec('hamachi login');
  }

  /// Logout from the Hamachi network
  ///
  static String Logout() {
    return _exec('hamachi logout');
  }

  /// Internal call to actually get the list
  ///
  static String _getHamachiList() {
    var results = Process.runSync(
      '/bin/bash',
      ['-c', 'hamachi list'],
      runInShell: true,
      // mode:ProcessStartMode.normal
    );
    return results.stdout.toString();
  }

  /// Internal call to get the status of a hamachi client
  ///
  static String _getHamachiPeer(String hamId) {
    return Process.runSync('/bin/bash', ['-c', 'hamachi peer ${hamId}'],
            runInShell: true)
        .stdout
        .toString();
  }

  /// Internal call to a generic command
  ///
  static String _exec(String cmd) {
    return Process.runSync('/bin/bash', ['-c', cmd], runInShell: true)
        .stdout
        .toString();
  }
}
