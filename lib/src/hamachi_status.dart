import 'package:hamachi/src/hamachi_connection.dart';

/// Model to hold HamachiStatus information
///
/// The results is the parsing of
/// ```
/// hamachi peer 333-333-333
///     client id       : 333-333-333
///     nickname        : mycomputer
///     VPN alias       : not set
///     connection      : direct
///
///     authentication : completed
///     encryption     : enabled
///     compression    : disabled
///     VPN status     : ok
///     VIP address    : 25.x.x.x    2620:x::x:x
///
///     via server  ok       TCP  n/a
///   * direct      ok       UDP  69.x.x.x:x
///
/// ```
/// Which will be returned as:
///
/// ```
/// var status = Hamachi.getHamachiStatus('333-333-333');
///
/// /*
/// status.id == '333-333-333'
/// status.alias == 'mycomputer'
/// status.vpnAlias =='not set'
/// status.connection == 'direct'
/// status.authentication == 'completed'
/// status.encryption == 'enabled'
/// status.compression == 'disabled'
/// status.vpnStauts == 'ok'
/// status.vipAddress == '25.x.x.x    2620:x::x:x'
/// status.connections[0].active == false
/// status.connections[1].active == true
/// ...
/// */
/// ```
///
class HamachiStatus {
  static final reStatus = RegExp(r'^(?<key>.{19,20}):(?<value>.*)$');
  String id;
  String alias;
  String vpnAlias;
  String connection;
  String authentication;
  String encryption;
  String compression;
  String vpnStatus = 'disconnected';
  String vipAddress;
  var connections = <HamachiConnection>[];

  /// constructor
  ///
  HamachiStatus();

  /// Creates a HamachiStatus object from List of lines
  ///
  factory HamachiStatus.fromList(List<String> lines) {
    var status = HamachiStatus();
    lines.forEach((line) {
      //     authentication : completed
      var match = reStatus.firstMatch(line);
      if (match != null) {
        var key = match.namedGroup('key').trim();
        var value = match.namedGroup('value').trim();
        switch (key) {
          case 'client id':
            {
              status.id = value;
            }
            break;
          case 'nickname':
            {
              status.alias = value;
            }
            break;
          case 'VPN alias':
            {
              status.vpnAlias = value;
            }
            break;
          case 'connection':
            {
              status.connection = value;
            }
            break;
          case 'authentication':
            {
              status.authentication = value;
            }
            break;
          case 'encryption':
            {
              status.encryption = value;
            }
            break;
          case 'compression':
            {
              status.compression = value;
            }
            break;
          case 'VPN status':
            {
              status.vpnStatus = value;
            }
            break;
          case 'VIP address':
            {
              status.vipAddress = value;
            }
            break;
        }
      } else {
        var connection = HamachiConnection.fromString(line);
        if (connection != null) {
          status.connections.add(connection);
        }
      }
    });
    return status;
  }
  @override
  String toString() {
    return 'HamachiStatus: ${id} ${alias} ${vpnAlias} ${connection} ${authentication} ${encryption} ${compression} ${vpnStatus} ${vipAddress} ${connections}';
  }
}
