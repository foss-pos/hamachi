import 'package:hamachi/src/hamachi_network.dart';

/// Model to hold information about Hamachi clients
///
class HamachiComputer {
  /// name corresponds to the hamachi client name
  ///
  String name;

  /// id corresponds to the unique hamachi client id
  String id;

  /// ipv4 corresponds to the 25.x.x.x address
  String ipv4;

  /// alias corresponds to the hamachi alias (not set by default)
  String alias;

  /// ipv6 corresponds to the ipv6 hamachi address
  String ipv6;

  /// type corresponds to the type of connection (direct/relayed)
  String type;

  /// protocol corresponds to the hamachi protocol used (UDP/TCP)
  String protocol;

  /// proxy corresponds to the proxy currently being used by hamachi
  String proxy;

  /// Description of the network to which this computer belongs
  HamachiNetwork network;

  /// Constructor
  ///
  HamachiComputer();

  /// Takes a regex match and creates a HamachiComputer object
  ///
  factory HamachiComputer.fromMatch(RegExpMatch match) {
    var computer = HamachiComputer();
    computer.name = match.namedGroup('name').trim();
    computer.id = match.namedGroup('id').trim();
    computer.ipv4 = match.namedGroup('ipv4').trim();
    computer.alias = match.namedGroup('alias').trim();
    if (match.namedGroup('ipv6') != null) {
      computer.ipv6 = match.namedGroup('ipv6').trim();
    }
    if (match.namedGroup('type') != null) {
      computer.type = match.namedGroup('type').trim();
    }
    if (match.namedGroup('protocol') != null) {
      computer.protocol = match.namedGroup('protocol').trim();
    }
    if (match.namedGroup('proxy') != null) {
      computer.proxy = match.namedGroup('proxy').trim();
    }
    // print(computer);
    return computer;
  }
  @override
  String toString() {
    return 'HamachiComputer: ${name} ${id} ${ipv4} ${alias} ${ipv6} ${type} ${protocol} ${proxy} ${network}';
  }
}
