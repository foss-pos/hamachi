import 'package:hamachi/hamachi.dart';

void main() {
  print('-- hamachi login --');
  print(Hamachi.Login());
  print('-- hamachi clients list --');
  Hamachi.getList().forEach((element) {
    print(element);

    var computer = Hamachi.getHamachiComputer(element.id);
    // element and computer should be identical, this code
    // is just an example of the api
    print(computer.name);
    // computer_name

    // other computer properties
    // computer.name = 'computer_name'
    // computer.id = '234-321-233'
    // computer.ipv4 = '25.x.x.x'
    // computer.alias = 'not set'
    // computer.ipv6 = '2530:xxxxx'
    // computer.type = 'direct'
    // computer.protocol = 'UDP'
    // computer.proxy = '76.x.x.x'
    // computer.network.id = '432-432-334'
    // computer.network.name = 'network1'
    // computer.network.capacity = '3/5'
    // computer.network.type = 'Free'
    // computer.network.owner = 'owner@mail.com'

    var status = Hamachi.getHamachiStatus(element.id);
    print(status.vpnStatus);
    print(status);
    // ok
  });
  print('-- hamachi network list --');
  Hamachi.getNetworkList().forEach((element) {
    print(element);
  });

  print('-- just one network --');
  print(Hamachi.getList('Network2'));

  print('-- hamachi logout --');
  // print(Hamachi.Logout());
}
