## 0.0.1

- Initial push, created by Felipe Polo-Wood

## 0.0.2

- Improved publishing standards

## 0.0.3

- More documentation

## 0.0.4

- Changed the result of Hamachi.getHamachiStatus() from String to the HamachiStatus
- Added Hamachi.getNetworkList()
- Added optional parameter to Hamachi.getList([String networkName]) to return members of a specific network

## 0.0.5

- Fixed missing .toList() for filtering .getList on specific network

## 1.0.0

- Added Hamachi.Login and Hamachi.Logout
- Added clarification on README about LogMeIn Hamachi and platforms supported
