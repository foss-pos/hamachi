import 'package:hamachi/hamachi.dart';
import 'package:test/test.dart';

void main() {
  group('A group of tests', () {
    // setUp(() {
    //   Hamachi.parseList();
    // });

    test('get a list should return a valid list', () {
      expect(Hamachi.getList().runtimeType, <HamachiComputer>[].runtimeType);
    });

    test('get status should return a valid status', () {
      expect(Hamachi.getHamachiStatus('333-333-333').runtimeType,
          HamachiStatus().runtimeType);
    });
  });
}
