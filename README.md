[![Pub Package](https://img.shields.io/pub/v/hamachi.svg)](https://pub.dev/packages/hamachi)

# Overview
A library for Dart developers to access information about Hamachi clients.

The hamachi library is an API to the great LogMeIn Hamachi product (VPN.net), but
is NOT sponsored, supported or in any way related to the LogMeIn product or company.

This library can be used to create a UI (like Haguichi), or a tool for management
of your hamachi connected computers (like hamctl).

## Dependency
The library uses the hamachi executable to access the information on the network, so
it would only be useful on platforms where the hamachi executable is available 
(Windows, Linux and Mac), but so far it has only been tested on Mac.

## Usage

A simple usage example:
```shell
dart pub add hamachi

or 

dart pub upgrade hamachi
```

or edit your pubspec.yaml
```yaml
dependencies:
  hamachi: ^1.0.0
```

On your .dart file
```dart
import 'package:hamachi/hamachi.dart';

main() {
  print('${Hamachi.getNetworkList()}');
  print('${Hamachi.getList()}');
  print('${Hamachi.getList("myNetwork"}');
  print('${Hamachi.getHamachiComputer("222-222-222")}');
  print('${Hamachi.getHamachiStatus("222-222-222")}');
}
```

## Features and bugs

Please file feature requests and bugs at the [issue tracker][tracker].


## License

This library is licensed under the BSD License, see license for details [license].

[tracker]: https://gitlab.com/foss-pos/hamachi/-/issues
[license]: https://gitlab.com/foss-pos/hamachi/-/blob/master/LICENSE